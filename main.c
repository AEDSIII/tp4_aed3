#include "main.h"
#include "funcoes.h"


int main(int argc, char *argv[])
{

    Thread *thread= (Thread  *)malloc(sizeof(Thread ));;
    int opt;

    criar_Grafo(&thread->grafo, argv[2]);

    while( (opt = getopt(argc, argv, "f:b:p")) > 0 )
    {
      switch ( opt )
      {

        case 'f': // opção -i
        if(pthread_create(&thread->t1, NULL, coloracao_FaberCastel, (void *)thread))
        {
          fprintf(stderr, "Error creating thread1\n");
          return 1;
        }

        if(pthread_create(&thread->t2, NULL, coloracao_FaberCastel, (void *)thread))
        {
          fprintf(stderr, "Error creating thread2\n");
          return 1;
        }

        if(pthread_create(&thread->t3, NULL, coloracao_FaberCastel, (void *)thread))
        {
          fprintf(stderr, "Error creating thread3\n");
          return 1;
        }

        if(pthread_create(&thread->t4, NULL, coloracao_FaberCastel, (void *)thread))
        {
          fprintf(stderr, "Error creating thread3\n");
          return 1;
        }


        pthread_join (thread->t1, NULL);
        pthread_join (thread->t2, NULL);
        pthread_join (thread->t3, NULL);
        pthread_join (thread->t4, NULL);
        break;

        case 'b': // opção -i
        if(pthread_create(&thread->t1, NULL, coloracao_BIC, (void *)thread))
        {
          fprintf(stderr, "Error creating thread1\n");
          return 1;
        }

        if(pthread_create(&thread->t2, NULL, coloracao_BIC, (void *)thread))
        {
          fprintf(stderr, "Error creating thread2\n");
          return 1;
        }

        if(pthread_create(&thread->t3, NULL, coloracao_BIC, (void *)thread))
        {
          fprintf(stderr, "Error creating thread3\n");
          return 1;
        }

        if(pthread_create(&thread->t4, NULL, coloracao_BIC, (void *)thread))
        {
          fprintf(stderr, "Error creating thread3\n");
          return 1;
        }

        pthread_join (thread->t1, NULL);
        pthread_join (thread->t2, NULL);
        pthread_join (thread->t3, NULL);
        pthread_join (thread->t4, NULL);

        break;

        default:
        printf("Erro na entrada\n");
        return 0;

      }
    }


    printf("%d\n",thread->grafo.quantidade_cor );
    desalocar_Grafo(&(thread->grafo));

    return 0;
}
