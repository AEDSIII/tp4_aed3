#include "main.h"
#include "funcoes.h"
int arquivo_vazio(char *arquivo)
{//*
    FILE *file = fopen(arquivo, "r");

    if(file == NULL)
        return 0;

    fseek(file, 0, SEEK_END); //
    int size = ftell(file);
    fclose(file);
    //*/
    return size;
}

void criar_Grafo(Grafo *grafo, char *nome_arquivo)
{//*
  if(!arquivo_vazio(nome_arquivo))
  {
    return;
  }
  grafo->quantidade_cor = 1;
  FILE *file = fopen(nome_arquivo, "r");
  int i, j, k;

  fscanf(file,"%d", &grafo->vertice);//pega a int do arquivo e coloca na variavel

  //Alocando o grafo
  grafo->mgrafo = (int **)malloc(grafo->vertice * sizeof(int *));
  for(i = 0;i < grafo->vertice; i++)
  {
    grafo->mgrafo[i] = (int *)malloc(grafo->vertice * sizeof(int));
    for(j = 0;j < grafo->vertice; j++)
    {
      grafo->mgrafo[i][j] = 0;
    }
  }

  // definindo aonde existe o aresta
  for(i = 0; i < (grafo->vertice * grafo->vertice)/2; i++)
  {
    fscanf(file,"%d", &j);//pega a int do arquivo e coloca na variavel
    fscanf(file,"%d", &k);//pega a int do arquivo e coloca na variavel

    if((j != k) && (j < grafo->vertice) && (k < grafo->vertice) ) //verificando selfloops
    {
      grafo->mgrafo[j][k]=1;
      grafo->mgrafo[k][j]=1;
    }else if((j >= grafo->vertice) || (k >= grafo->vertice))
    {
      printf("i %d k %d grafo %d\n",i,k,grafo->vertice );
      printf("Erro na composição do grafo na linha %d \n", i+1 );
      return;
    }
  }
  //*/
  return;
}

void desalocar_Grafo(Grafo *grafo)
{//*

  int i;

  for(i = 0;i < grafo->vertice; i++)
  {
      free(grafo->mgrafo[i]);
  }
    //*/
    return;

}

void printar_Grafo(Grafo *grafo)
{//*
  int i,j;

  printf("%d\n",grafo->vertice );
  for(i = 0;i < grafo->vertice; i++)
  {
    for(j = 0;j < grafo->vertice; j++)
    {
      printf("%d |", grafo->mgrafo[i][j]);
    }
    printf("\n");
  }

  //*/
  return;
}

//-------------------COLORAÇÃO DE GRAFOS----------------------------

void *coloracao_FaberCastel(void *thread_aux)
{//*
  int i,j,k,t_inicial, t_final;
  Thread *thread = (Thread *) thread_aux;
  Grafo *grafo = &(thread->grafo);

  pthread_mutex_init (&(thread->trava), NULL);


  if(pthread_equal (pthread_self (), thread->t1))
  {
    t_inicial = 0;
    t_final = (int) (grafo->vertice*(1/4.0));

  }else if(pthread_equal (pthread_self (), thread->t2))
  {
    t_inicial = (int) (grafo->vertice*(1/4.0))+1;;
    t_final = (int) (grafo->vertice*(1/2.0));

  }else if(pthread_equal (pthread_self (), thread->t3))
  {
    t_inicial = (int) (grafo->vertice*(1/2.0))+1;;
    t_final = (int) (grafo->vertice*(3/4.0));
  }else if(pthread_equal (pthread_self (),thread->t4 ))
  {
    t_inicial = (int) (grafo->vertice*(3/4.0))+1;;
    t_final = (int) (grafo->vertice);
  }



  for(i = t_inicial;i < t_final; i++)
  {
    for(j = i+1 ;j < t_final; j++)
    {
      if(grafo->mgrafo[i][j] != 0)
      {
        for(k = j+1 ; k < t_final; k++)
        {
          if(grafo->mgrafo[i][j] == grafo->mgrafo[i][k])
          {
            pthread_mutex_lock (&(thread->trava));
            grafo->mgrafo[i][k] ++;
            if (grafo->mgrafo[i][k] > grafo->quantidade_cor)
            {
              grafo->quantidade_cor = grafo->mgrafo[i][k];
            }
            i = 0;
            j = 0;      //Faz a função voltar no inicio e ir verificando se tem algum visinho
            k = t_final;
            pthread_mutex_unlock (&(thread->trava));
          }
        }
      }
    }
  }

  pthread_mutex_lock (&(thread->trava));
  grafo->quantidade_cor++; // e acrecentado mais um para poder garantir a cor do raiz
  pthread_mutex_unlock (&(thread->trava));

  pthread_mutex_destroy (&(thread->trava));
  //*/
  return NULL;
}

void *coloracao_BIC(void *thread_aux)
{//*

    int i,j,t_inicial,t_final;
    Thread *thread = (Thread *) thread_aux;
    Grafo *grafo = &(thread->grafo);
    int *vgrafos = (int *)malloc(grafo->vertice * sizeof(int));
    pthread_mutex_init (&(thread->trava), NULL);

    if(pthread_equal (pthread_self (), thread->t1))
    {
      t_inicial = 0;
      t_final = (int) (grafo->vertice*(1/4.0));

    }else if(pthread_equal (pthread_self (), thread->t2))
    {
      t_inicial = (int) (grafo->vertice*(1/4.0))+1;;
      t_final = (int) (grafo->vertice*(1/2.0));

    }else if(pthread_equal (pthread_self (), thread->t3))
    {
      t_inicial = (int) (grafo->vertice*(1/2.0))+1;;
      t_final = (int) (grafo->vertice*(3/4.0));
    }else if(pthread_equal (pthread_self (),thread->t4 ))
    {
      t_inicial = (int) (grafo->vertice*(3/4.0))+1;;
      t_final = (int) (grafo->vertice);
    }

    if(pthread_equal (pthread_self (), thread->t1))
    {
      int *vgrafos = (int *)malloc(grafo->vertice * sizeof(int));

      for(i = 0;i < grafo->vertice; i++)
      {
        pthread_mutex_lock (&(thread->trava));
        vgrafos[i] = 0;
        pthread_mutex_unlock (&(thread->trava));
      }
    }

    for(i = t_inicial;i < t_final; i++)
    {
      for(j = i+1 ;j < t_final; j++)
      {
        if( (grafo->mgrafo[i][j] == 1) && (vgrafos[i] == vgrafos[j]) ) //verifica se existe vertice e tem a mesma cor
        {
          pthread_mutex_lock (&(thread->trava));
          vgrafos[j]++;
          pthread_mutex_unlock (&(thread->trava));
        }
      }
    }

    for(i = t_inicial;i < t_final; i++)
    {
        if(grafo->quantidade_cor < vgrafos[i])
        {
          pthread_mutex_lock (&(thread->trava));
          grafo->quantidade_cor = vgrafos[i];
          pthread_mutex_unlock (&(thread->trava));
        }
    }
    pthread_mutex_lock (&(thread->trava));
    grafo->quantidade_cor++;
    pthread_mutex_unlock (&(thread->trava));

    pthread_mutex_destroy (&(thread->trava));
    free(vgrafos);
    //*/
    return NULL;
}
