#ifndef funcao
#define funcao

int arquivo_vazio(char *arquivo);
/*Verifica se o arquivo esta vAzio.
 *
 *param: recebe o nome do arquivo que deseja abrir
 *
 *return: 0 se o arquivo estiver vAzio, ou qualquer outro valor se não estiver vAzio.
*/

void criar_Grafo(Grafo *grafo, char *nome_arquivo);
/*Funcao para alocar memoria do grafo e preenchelo;
 *
 *param: recebe o ponteiro da struct composicao e coloca os valores na composicao
*/

void desalocar_Grafo(Grafo *grafo);
/*Funcao desalocar a memoria do grafo;
 *
 *param: recebe o ponteiro da struct grafo e cria o grafo
*/
//-------------------COLORAÇÃO DE GRAFOS----------------------------
void *coloracao_FaberCastel(void *grafo_aux) ;
/*Que verifica quantidade de coloração de grafo usando força bruta;
 *
 *param: recebe o ponteiro da struct grafo e cria o grafo
*/

void *coloracao_BIC(void *thread_aux);
/*Que verifica quantidade de coloração de grafo usando Heuristica;
 *
 *param: recebe o ponteiro da struct grafo e cria o grafo
*/

#endif
